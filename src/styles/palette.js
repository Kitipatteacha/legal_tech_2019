import { red } from '@material-ui/core/colors';

const palette = {
    primary: { main: '#2196f3' }, // blue
    secondary: { main: '#00b2a6' }, // green
    error: { main: red.A400 },
    background: { default: '#F2F2F2' },
    button: { main: '#587CF9' },
    textPrimary: {
        main: '#000000', // black
        dark: '#6D7278', // dark gray
        light: '#FFFFFF', //white
    },
};

export default palette;
