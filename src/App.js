import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import NavbarComponent from './components/Navbar/Navbar';
import homepage from './containers/Home/Home';
import selectDispute from './containers/SelectDispute/SelectDispute.jsx';
import QuestionController from './containers/QuestionController/QuestionController';
import { MuiThemeProvider } from '@material-ui/core/styles';
import THEME from './styles/theme';

function App() {
    return (
        <div
            style={{
                height: '100vh',
                display: 'flex',
                flexDirection: 'column',
            }}
        >
            <BrowserRouter>
                <NavbarComponent />
                <MuiThemeProvider theme={THEME}>
                    <Switch>
                        <Route path='/dispute' component={selectDispute} />
                        <Route
                            path='/question/:dispute'
                            component={QuestionController}
                        />
                        <Route path='/' component={homepage} />
                    </Switch>
                </MuiThemeProvider>
            </BrowserRouter>
        </div>
    );
}

export default App;
