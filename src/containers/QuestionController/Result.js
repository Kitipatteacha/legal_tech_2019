import React, { Fragment } from 'react';
import { COVID_DISPUTE, COMPENSATION_DISPUTE } from './string_mapper';
import { Button, CardContent, Card, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PageviewIcon from '@material-ui/icons/Pageview';

const useStyles = makeStyles((theme) => ({
    card: {
        justifyContent: 'center',
        borderRadius: 15,
        backgroundColor: '#f0f6ff',
        margin: theme.spacing(2),
    },
    title: {
        display: 'flex',
        alignItems: 'center',
    },
    icon: {
        marginRight: 20,
    },
}));

export default function Result({ result: { dispute, payload, links } }) {
    const classes = useStyles();
    let data = <p></p>;

    if (dispute === COMPENSATION_DISPUTE) {
        data = <p>กรณีของคุณไม่เข้าข่ายชดเชย</p>;
        const { section, wage_per_day, multiplier, compensation } = payload;
        if (section !== '') {
            data = (
                <Fragment>
                    <p>กรณีของคุณเข้าข่ายกฎหมาย มาตรา {section}</p>

                    <p>
                        ค่าจ้างต่อวันโดยเฉลี่ย{' '}
                        {parseFloat(wage_per_day).toFixed(2)} บาท
                    </p>
                    <p>จากมาตรานี้ จะได้ค่าชดเชย {multiplier} วัน</p>

                    <p>
                        คุณควรจะได้รับเงินค่าชดเชยจำนวน{' '}
                        {compensation.toLocaleString()} บาท
                    </p>
                </Fragment>
            );
        }
    } else if (dispute === COVID_DISPUTE) {
        const { message } = payload;
        data = (
            <Fragment>
                <p>{message}</p>
            </Fragment>
        );
    }

    const navigations = links.map(({ name, to, quote }) => {
        return (
            <div key={name}>
                <p>{quote}</p>
                <Button color='primary' variant='contained' href={to}>
                    {name}
                </Button>
            </div>
        );
    });

    return (
        <div>
            <Card className={classes.card}>
                <CardContent>
                    <div className={classes.title}>
                        <PageviewIcon
                            fontSize='large'
                            className={classes.icon}
                        />
                        <Typography variant='h4'>Result</Typography>
                    </div>

                    {data}
                </CardContent>
            </Card>
            <Card className={classes.card}>
                <CardContent>{navigations}</CardContent>
            </Card>
        </div>
    );
}
