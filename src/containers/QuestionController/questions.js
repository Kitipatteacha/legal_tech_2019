import {
  PROBLEM_A,
  PROBLEM_B,
  PROBLEM_C,
  YES,
  NO,
  COMPENSATION_DISPUTE,
  COVID_DISPUTE,
} from './string_mapper';

export function getNextPage(dispute, answer_collection) {
  const last_answers = answer_collection[answer_collection.length - 1];

  const { tag, answers } = last_answers;

  let answer;
  if (dispute === COVID_DISPUTE) {
    switch (tag) {
      case 'problem':
        answer = findAnswerByLabel('problem', answers);
        switch (answer) {
          case PROBLEM_A:
            return getPage(dispute, 'pay_cut');
          case PROBLEM_B:
            return getPage(dispute, 'WFH');
          case PROBLEM_C:
            return getPage(dispute, 'result');
          default:
            throw Error('error');
        }

      case 'pay_cut':
        answer = findAnswerByLabel('pay_cut', answers);
        switch (answer) {
          case YES:
            return getPage(dispute, 'agreement');
          case NO:
            return getPage(dispute, 'result');
          default:
            throw Error('error');
        }

      case 'agreement':
        return getPage(dispute, 'result');

      case 'WFH':
        return getPage(dispute, 'result');

      default:
        throw Error('ERROR');
    }
  } else if (dispute === COMPENSATION_DISPUTE) {
    switch (tag) {
      case 'employee_type':
        answer = findAnswerByLabel('employee_type', answers);
        if (answer === 'ตามผลงาน') {
          return getPage(dispute, 'benefit'); // should be others
        }
        return getPage(dispute, 'benefit');

      case 'benefit':
        answer = findAnswerByLabel('benefit', answers);
        if (answer === 'มี') return getPage(dispute, 'wage'); // should be others

        // check wage_day or wage_month
        const employee_type = findAnswersByTag(
          'employee_type',
          answer_collection
        )[0].answer;
        if (employee_type === 'รายวัน') return getPage(dispute, 'wage_day');
        return getPage(dispute, 'wage_month');

      case 'wage_day':
        // const answer = findAnswerByLabel('wage', answers);
        return getPage(dispute, 'day_numbers');

      case 'wage_month':
        // const answer = findAnswerByLabel('wage', answers);
        return getPage(dispute, 'day_numbers');

      case 'day_numbers':
        // const answer = findAnswerByLabel('start_date', answers)
        return getPage(dispute, 'result');

      default:
        throw Error('ERROR');
    }
  }
}

export function getPage(dispute, label) {
  switch (dispute) {
    case COMPENSATION_DISPUTE:
      switch (label) {
        case 'employee_type':
          return {
            tag: 'employee_type',
            question: 'ท่านคือลูกจ้างแบบใด',
            inputs: [
              {
                label: 'employee_type',
                answer_type: 'select',
                default_answer: 'รายวัน',
                choices: ['รายวัน', 'รายเดือน', 'ตามผลงาน'],
              },
            ],
          };
        case 'benefit':
          return {
            tag: 'benefit',
            question:
              'มีค่าคอมมิชชั่น/เบี้ยเลี้ยง/สวัสดิการเพิ่มเติมอื่นๆที่จ่ายพร้อมกับค่าจ้างอีกหรือไม่?',
            inputs: [
              {
                label: 'benefit',
                answer_type: 'select',
                default_answer: 'yes',
                choices: ['yes', 'no'],
              },
            ],
          };
        case 'wage_month':
          return {
            tag: 'wage_month',
            question: 'ค่าจ้างต่อเดือน?',
            inputs: [
              {
                label: 'wage_month',
                answer_type: 'text',
                default_answer: 0,
              },
            ],
          };
        case 'wage_day':
          return {
            tag: 'wage_day',
            question: 'ค่าจ้างต่อวัน?',
            inputs: [
              {
                label: 'wage_day',
                answer_type: 'text',
                default_answer: 0,
              },
            ],
          };
        case 'day_numbers':
          return {
            tag: 'day_numbers',
            question: 'ทำงานติดต่อกันตั้งแต่วันที่เท่าไหร่จนถึงวันที่เท่าไหร่?',
            inputs: [
              {
                label: 'start_date',
                answer_type: 'date_picker',
              },
              {
                label: 'end_date',
                answer_type: 'date_picker',
              },
            ],
          };
        case 'result':
          return 'result';
        default:
          return {
            tag: 'employee_type',
            question: 'ท่านคือลูกจ้างแบบใด',
            inputs: [
              {
                label: 'employee_type',
                answer_type: 'select',
                default_answer: 'รายวัน',
                choices: ['รายวัน', 'รายเดือน'],
              },
            ],
          };
      }
    case COVID_DISPUTE:
      switch (label) {
        case 'problem':
          return {
            tag: 'problem',
            question: 'ปัญหาที่คุณพบเนื่องจากสถานการณ์การระบาดของโรค Covid-19',
            inputs: [
              {
                label: 'problem',
                answer_type: 'select',
                default_answer: PROBLEM_A,
                choices: [PROBLEM_A, PROBLEM_B, PROBLEM_C],
              },
            ],
          };
        case 'pay_cut':
          return {
            tag: 'pay_cut',
            question: 'คุณถูกลดค่าจ้างหรือไม่?',
            inputs: [
              {
                label: 'pay_cut',
                answer_type: 'select',
                default_answer: YES,
                choices: [YES, NO],
              },
            ],
          };
        case 'agreement':
          return {
            tag: 'agreement',
            question: 'มีการตกลงว่าจะลดค่าจ้างกันก่อนหรือไม่?',
            inputs: [
              {
                label: 'agreement',
                answer_type: 'select',
                default_answer: YES,
                choices: [YES, NO],
              },
            ],
          };
        case 'WFH':
          return {
            tag: 'WFH',
            question: 'บริษัทยังมีให้ Work From Home หรือไม่?',
            inputs: [
              {
                label: 'WFH',
                answer_type: 'select',
                default_answer: YES,
                choices: [YES, NO],
              },
            ],
          };
        case 'result':
          return 'result';
        default:
          // no provided label, go to first question
          return {
            tag: 'problem',
            question: 'ปัญหาที่คุณพบเนื่องจากสถานการณ์การระบาดของโรค Covid-19',
            inputs: [
              {
                label: 'problem',
                answer_type: 'select',
                default_answer: PROBLEM_A,
                choices: [PROBLEM_A, PROBLEM_B, PROBLEM_C],
              },
            ],
          };
      }
    default:
      break;
  }
}

const findAnswerByLabel = (label, arr) => {
  const obj = arr.find((item) => {
    return item.label === label;
  });
  return obj.answer;
};

const findAnswersByTag = (tag, answer_collection) => {
  const obj = answer_collection.find((item) => {
    return item.tag === tag;
  });
  return obj.answers;
};
