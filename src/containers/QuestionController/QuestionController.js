import React, { Component } from 'react';
import Question from '../../components/Question/Question';
import process_result from './process_result';
import Result from './Result';
import { getPage, getNextPage } from './questions';
export default class QuestionController extends Component {
    state = {
        question_obj: null,
        dispute: this.props.match.params.dispute,
        answer_collection: [],
        show_result: false,
        result: null,
    };

    setNextQuestion = (question) => {
        this.setState({
            question_obj: question,
        });
    };

    getNextQuestion = () => {
        const page = getNextPage(
            this.state.dispute,
            this.state.answer_collection
        );
        if (page === 'result') {
            return this.getResult();
        }
        this.setNextQuestion(page);
    };

    // componentWillMount = () => {
    //   if (!this.props.location.dispute) {
    //     this.props.history.push('/'); // redirect to home if refresh a page
    //   }
    //   this.setState({
    //     dispute: this.props.location.dispute,
    //   });
    // };

    componentDidMount = () => {
        const question = getPage(this.state.dispute);
        this.setNextQuestion(question);
    };

    getResult = () => {
        this.setState({
            show_result: true,
            result: process_result(
                this.state.dispute,
                this.state.answer_collection
            ),
        });
    };

    ansCollect = (answers) => {
        // tag answer with question tag
        const new_answers = {
            tag: this.state.question_obj.tag,
            answers: answers,
        };

        let answer_collection = [...this.state.answer_collection];
        answer_collection.push(new_answers);

        this.setState(
            {
                answer_collection: answer_collection,
            },
            () => {
                this.getNextQuestion();
            }
        );
    };

    render() {
        let question = <p></p>;
        if (this.state.question_obj && !this.state.show_result) {
            question = (
                <Question
                    question_obj={this.state.question_obj}
                    ansCollect={this.ansCollect}
                />
            );
        }
        let result = <p></p>;
        if (this.state.show_result) {
            result = <Result result={this.state.result} />;
        }
        return (
            <div
                style={{
                    color: 'white',
                    backgroundColor: '#202020',
                    display: 'flex',
                    justifyContent: 'center',
                    textAlign: 'center',
                    minHeight: '100vh',
                }}
            >
                <div style={{ marginTop: '10vh' }}>{question}</div>
                {result}
            </div>
        );
    }
}
