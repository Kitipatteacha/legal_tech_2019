export const PROBLEM_A = 'บริษัทให้ work from home';
export const PROBLEM_B = 'สถานประกอบการถูกสั่งปิดชั่วคราวเนื่องจากคำสั่งของรัฐ';
export const PROBLEM_C = 'สถานประกอบการเลิกจ้างเนื่องจากการระบาดของโรค';

export const YES = 'Yes';
export const NO = 'No';

export const COVID_DISPUTE = 'covid';
export const COMPENSATION_DISPUTE = 'compensation';
