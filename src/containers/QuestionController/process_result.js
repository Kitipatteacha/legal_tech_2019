import {
  PROBLEM_A,
  PROBLEM_B,
  PROBLEM_C,
  YES,
  COMPENSATION_DISPUTE,
  COVID_DISPUTE,
} from './string_mapper';

export default function process_result(dispute, answer_collection) {
  let payload;
  let links = [
    {
      name: 'หาทนาย',
      to:
        'https://www.google.com/search?q=%E0%B8%AB%E0%B8%B2%E0%B8%97%E0%B8%99%E0%B8%B2%E0%B8%A2%E0%B8%AD%E0%B8%AD%E0%B8%99%E0%B9%84%E0%B8%A5%E0%B8%99%E0%B9%8C&rlz=1C1SQJL_enTH858TH858&oq=%E0%B8%AB%E0%B8%B2%E0%B8%97%E0%B8%99%E0%B8%B2',
      quote: 'มีปัญหาเพิ่มเติม อยากถามนักกฎหมายเพิ่มเติมอีกใช่ไหม',
    },
    {
      name: 'หางาน',
      to: 'https://th.jobsdb.com/th/th',
      quote: 'ลองหาความท้าทายใหม่ๆไหม',
    },
    {
      name: 'ยื่นเลย',
      to: 'http://s90.labour.go.th/e_request/login.php',
      quote: 'ยื่นคำร้องให้กับกรมแรงงานทางออนไลน์ได้แล้วนะ',
    },
  ];
  switch (dispute) {
    case COMPENSATION_DISPUTE:
      // compensation algorithm
      let employee_type = '';
      let compensation = 0;
      let wage_per_day = 0;
      let multiplier = 0;
      let start_date;
      let end_date;

      for (const i in answer_collection) {
        const question = answer_collection[i];

        const { answers } = question;
        for (const j in answers) {
          const label = answers[j].label;
          const answer = answers[j].answer;
          if (label === 'employee_type') {
            employee_type = answer;
          } else if (label === 'wage_day') {
            wage_per_day = answer;
          } else if (label === 'wage_month') {
            wage_per_day = answer / 30;
          } else if (label === 'start_date') {
            start_date = answer;
          } else if (label === 'end_date') {
            end_date = answer;
          }
        }
      }

      let working_day = Math.floor(
        (end_date - start_date) / (1000 * 60 * 60 * 24)
      );
      let section;
      if (working_day > 20 * 365) {
        multiplier = 400;
        section = '118(6) ทำงานติดต่อกันยี่สิบปีขึ้นไป';
      } else if (working_day > 10 * 365) {
        multiplier = 300;
        section = '118(5) ทำงานติดต่อกันสิบปีขึ้นไป แต่ไม่ครบยี่สิบปี';
      } else if (working_day > 6 * 365) {
        multiplier = 240;
        section = '118(4) ทำงานติดต่อกันหกปีขึ้นไป แต่ไม่ครบสิบปี';
      } else if (working_day > 3 * 365) {
        multiplier = 180;
        section = '118(3) ทำงานติดต่อกันสามปัขึ้นไป แต่ไม่ครบหกปี';
      } else if (working_day > 365) {
        multiplier = 90;
        section = '118(2) ทำงานติดต่อกันหนึ่งปี แต่ไม่ครบสามปี';
      } else if (working_day > 120) {
        multiplier = 30;
        section = '118(1) ทำงานติดต่อกันครบ 120 วัน แต่ไม่ครบหนึ่งปี';
      } else {
        multiplier = 0;
        section = '';
      }

      compensation = wage_per_day * multiplier;
      payload = {
        employee_type,
        working_day,
        multiplier,
        wage_per_day,
        compensation,
        section,
      };

      return { dispute, payload, links };

    case COVID_DISPUTE:
      let problem;
      let pay_cut;
      let agreement;
      let WFH;

      for (const i in answer_collection) {
        const question = answer_collection[i];

        const { answers } = question;
        for (const j in answers) {
          const label = answers[j].label;
          const answer = answers[j].answer;
          if (label === 'problem') {
            problem = answer;
          } else if (label === 'pay_cut') {
            pay_cut = answer;
          } else if (label === 'agreement') {
            agreement = answer;
          } else if (label === 'WFH') {
            WFH = answer;
          }
        }
      }

      let message = '';

      if (problem === PROBLEM_A) {
        if (pay_cut === YES) {
          if (agreement === YES) {
            message =
              'เป็นการตกลงกันเพื่อเปลี่ยนแปลงเนื้อหาในสัญญาจ้างแรงงาน ซึ่งหากทั้งสองฝ่ายยินยอม (Mutual agreement) ก็สามารถทำได้ ดังนั้นกรณีนี้ลูกจ้างและนายจ้างทำถูกต้องตามกฎหมายแล้ว ทั้งนี้ความยินยอมดังกล่าวไม่จำต้องทำเป็นหนังสือก็ใช้ได้ เพราะสัญญาจ้างแรงงาน กฎหมายไม่ได้กำหนดแบบไว้ การแก้ไขสัญญาจึงไม่มีแบบเช่นกัน';
          } else {
            message =
              'กรณีที่เปลี่ยนสถานที่ทำงานจากในสถานประกอบการ มาให้ทำงานที่บ้าน และเปลี่ยนแปลงค่าจ้าง เป็นการที่นายจ้างต้องการเปลี่ยนแปลงเนื้อหาในสัญญาจ้างแรงงาน ซึ่งจะต้องได้รับความยินยอมจากลูกจ้างด้วย กรณีนี้ถ้าภายหลังลูกจ้างไม่ยินยอมให้ลดค่าจ้าง จะมีผลเท่ากับนายจ้างเลิกจ้าง นายจ้างมีหน้าที่จะต้องจ่ายค่าชดเชย';
            links = [...links, compensation_link];
          }
        } else {
          message =
            'สามารถทำได้ เป็นการตกลงกันเพื่อเปลี่ยนแปลงเนื้อหาในสัญญาจ้างแรงงานเกี่ยวกับสถานที่ทำงาน วันและเวลาทำงาน ซึ่งหากลูกจ้างตกลงยินยอมด้วยก็มีผลเป็นการแก้ไขสัญญา แต่ถ้าลูกจ้างไม่ยินยอม มีผลเท่ากับเป็นการเลิกจ้าง ลูกจ้างมีสิทธิในค่าชดเชย ตามพ.ร.บ.คุ้มครองแรงงาน มาตรา 118 *ทั้งนี้ความยินยอมดังกล่าวไม่จำต้องทำเป็นหนังสือก็ใช้ได้ เพราะสัญญาจ้างแรงงาน กฎหมายไม่ได้กำหนดแบบไว้ การแก้ไขสัญญาจึงไม่มีแบบเช่นกัน';
          links = [...links, compensation_link];
        }
      } else if (problem === PROBLEM_B) {
        if (WFH === YES) {
          message =
            'กรณีนี้ลูกจ้างยังทำงานให้แก่บริษัทอยู่ จะต้องได้รับค่าจ้างเต็มจำนวนตาม แต่ที่ตกลงกัน เพราะนายจ้างมีหน้าที่จ่ายสินจ้างให้แก่ลูกจ้างตลอดเวลาที่ลูกจ้างทำงานให้ (ประมวลกฎหมายแพ่งและพาณิชย์มาตรา 575)';
        } else {
          message =
            'คำสั่งของรัฐบาลที่ทำให้ต้องปิดสถานประกอบการถือเป็นเหตุสุดวิสัยตามประมวลกฎหมายแพ่งและพาณิชย์มาตรา 8 กรณีไม่ใช่ความผิดของฝ่ายลูกจ้างหรือนายจ้างฝ่ายใดฝ่ายหนึ่ง ทำให้นายจ้างสามารถอ้างเหตุสุดวิสัยไม่ต้องจ่ายค่าจ้างได้เพราะลูกจ้างไม่ได้ทำงานให้ ตามหลักสัญญาต่างตอบแทน แต่ลูกจ้างยังไม่มีสิทธิในค่าชดเชยและประกันสังคมเนื่องจากสัญญาจ้างยังไม่สิ้นสุด';
        }
      } else if (problem === PROBLEM_C) {
        message =
          'ลูกจ้างมีสิทธิตามกฎหมายดังต่อไปนี้คือ เลิกจ้างต้องจ่ายค่าชดเชย (เหตุโรคระบาดไม่ได้รับการยกเว้นในภาระการจ่ายค่าชดเชย) และสิทธิในเงินประกันสังคมกรณีว่างงาน';
        links = [...links, compensation_link, SSO_link];
      }
      payload = { message };
      return { dispute, payload, links };
    default:
      throw Error('Error');
  }
}

const compensation_link = {
  name: 'คำนวณค่าชดเชย',
  to: '/question/compensation',
  quote: 'ลองคิดค่าชดเชยดูก่อนไหม',
};
// const job_link = {
//   name: 'หางาน',
//   to: 'https://th.jobsdb.com/th/th',
//   quote: 'ลองหาความท้าทายใหม่ๆไหม',
// };
const SSO_link = {
  name: 'เช็คสิทธิประกันสังคม',
  to: 'https://www.sso.go.th',
  quote: 'เช็คสิทธิประกันสังคมกรณีว่างงาน',
};
