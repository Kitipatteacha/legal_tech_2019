import React from 'react';
import styles from './Home.module.css';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
const StyledButton = withStyles({
  root: {
    background: 'transparent',
    borderRadius: 3,
    border: 0,
    color: 'white',
    height: 48,
    padding: '0 30px',
    maxWidth: 200,
    fontSize: 20,
    margin: '40px auto',
    boxShadow: '0 2px 3px 2px rgba(255, 255, 255, .3)',
  },
  label: {
    textTransform: 'capitalize',
  },
})(Button);
export default function Home() {
  return (
    <div className={styles.wrapper}>
      <section className={styles.section_wrapper}>
        <div className={styles.banner_wrapper}>
          <div className={styles.banner} item xs={12} sm={12}>
            <Typography
              style={{
                fontSize: 'max(45px, 6vw)',
              }}
            >
              L A B O U R A L
            </Typography>
          </div>
          <div className={styles.banner} item xs={12} sm={12}>
            <Typography style={{ fontSize: 'max(18px, 1.5vw)' }}>
              L A B O U R &nbsp; W I T H &nbsp; L I B E R T Y.
            </Typography>
          </div>
          <StyledButton component={Link} to='/dispute'>
            Get Start
          </StyledButton>
        </div>
        <div></div>
        <div className={`${styles.wave} ${styles.wave1}`}></div>
        <div className={`${styles.wave} ${styles.wave2}`}></div>
        <div className={`${styles.wave} ${styles.wave3}`}></div>
        <div className={`${styles.wave} ${styles.wave4}`}></div>
      </section>
    </div>
  );
}
