import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Dispute from '../../components/Dispute';
import styles from './SelectDispute.module.css';
import test2Jpg from '../../images/test2.jpg';

const useStyles = makeStyles((theme) => ({
    body: {
        background: '#202020',
        display: 'flex',
        justifyContent: 'center',
    },
    paper: {
        width: '70%',
        minHeight: '80%',
        margin: '30px 60px 30px 60px',
        borderRadius: '50px',
    },
    content: {
        background: '#202020',
        display: 'flex',
        justifyContent: 'center',
    },
    disputes: {
        display: 'flex',
        maxHeight: '100%',
        width: '100%',
        flexFlow: 'row wrap',
        justifyContent: 'space-evenly',
        paddingBottom: '80px',
    },
    typo: {
        margin: '20px 0 0 0',
        color: 'white',
    },
    wavegroup: {
        height: '40px',
    },
}));
export default function SelectDispute() {
    const ITEM_PER_ROW = 1;
    const classes = useStyles({ ITEM_PER_ROW });
    const disputeList = [
        {
            image: test2Jpg,
            name: 'C O V I D',
            disabled: false,
            href: '/question/covid',
        },
        {
            image: test2Jpg,
            name: 'W A G E P A Y',
            disabled: false,
            href: '/question/compensation',
        },
        {
            image: test2Jpg,
            name: 'C H I L D R E N',
            disabled: true,
            href: '/',
        },
        {
            image: test2Jpg,
            name: 'W E L F A R E',
            disabled: true,
            href: '/',
        },
        {
            image: test2Jpg,
            name: 'O T H E R',
            disabled: true,
            href: '/',
        },
    ];
    const makeObjectListFit = (itemList, itemPerRow) => {
        const templateObject = { ...itemList[0] };
        Object.keys(templateObject).forEach(
            (key) => (templateObject[key] = null)
        );
        let fittedList = [...itemList];
        while (true) {
            // has a key problems
            if (fittedList.length % itemPerRow === 0) break;
            fittedList.push(templateObject);
        }
        return fittedList;
    };
    const dispute = makeObjectListFit(
        disputeList,
        ITEM_PER_ROW
    ).map((item, index) => (
        <Dispute
            key={`dispute-${index}`}
            image={item.image}
            name={item.name}
            disabled={item.disabled}
            href={item.href}
        />
    ));

    return (
        <div className={styles.wrapper}>
            <section className={styles.section_wrapper}>
                <div className={styles.banner_wrapper}>
                    <div className={styles.banner} item xs={12} sm={12}>
                        <Typography style={{ fontSize: 'max(45px, 6vw)' }}>
                            L A B O U R A L
                        </Typography>
                    </div>
                </div>
                <div className={classes.wavegroup}>
                    <div className={`${styles.wave} ${styles.wave1}`}></div>
                    <div className={`${styles.wave} ${styles.wave2}`}></div>
                    <div className={`${styles.wave} ${styles.wave3}`}></div>
                    <div className={`${styles.wave} ${styles.wave4}`}></div>
                </div>
            </section>
            <div
                style={{
                    height: '80vh',
                }}
            >
                <div className={styles.banner} item xs={12} sm={12}>
                    <Typography
                        style={{
                            fontSize: 'max(15px, 2.5vw)',
                            fontWeight: '900',
                            color: 'black',
                            marginTop: '100px',
                        }}
                    >
                        S E L E C T &nbsp; D I S P U T E
                    </Typography>
                </div>
                <div className={styles.banner} item xs={12} sm={12}>
                    <Typography
                        style={{
                            fontSize: 'max(11px, 1vw)',
                            color: 'black',
                            marginTop: '10px',
                            marginBottom: '10vh',
                        }}
                    >
                        Answer specific questions and get related documents,
                        case analysis.
                    </Typography>
                </div>
                <div className={classes.disputes}>{dispute}</div>
            </div>
        </div>
    );
}
