import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
// import IconButton from '@material-ui/core/IconButton';
// import MenuIcon from '@material-ui/icons/Menu';
// import logoImg from '../../images/logo.png';
import Typography from '@material-ui/core/Typography';
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    logo: {
        height: '10vh',
        width: 'auto',
    },
}));

const NavbarComponent = (props) => {
    const classes = useStyles();
    return (
        <div>
            <AppBar
                position='static'
                style={{ background: '#202020', boxShadow: 'none' }}
            >
                <Toolbar>
                    {/* <IconButton
                        edge='start'
                        className={classes.menuButton}
                        color='inherit'
                        aria-label='menu'
                    >
                        <MenuIcon />
                    </IconButton> */}
                    <Typography className={classes.title}>
                        {/* <img
                            className={classes.logo}
                            src={logoImg}
                            alt=''
                        ></img> */}
                        R N S K
                    </Typography>
                    <Button color='inherit' href='/'>
                        <Typography> H O M E</Typography>
                    </Button>
                    <Button color='inherit' href='/dispute'>
                        <Typography> D I S P U T E</Typography>
                    </Button>
                </Toolbar>
            </AppBar>
        </div>
    );
};
export default NavbarComponent;
