import React from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

const useStyles = makeStyles((theme) => ({
    body: (props) => ({
        marginTop: '3%',
        height: 'fit-content',
        width: '30%',
        minWidth: props.isMobile ? '90%' : '350px',
        margin: '10px 0',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        opacity: props.image && props.name ? 1 : 0,
    }),
    card: {
        display: 'flex',
        boxShadow: '4px  4px 8px #888888',
        [theme.breakpoints.down('md')]: {
            height: '125px',
            width: 'auto',
        },
    },
    img: {
        width: '50%',
    },
    typo: {
        margin: '30px 0 0 0',
        fontSize: '25px',
        fontWeight: '900',
    },
    detail: {
        margin: '20px 0 0 0',
        textAlign: 'center',
    },
    button: {
        margin: '20px 0 0 0',
        fontWeight: '800',
        boxShadow: '1px  1px 2px #888888',
        [theme.breakpoints.down('md')]: {
            height: '40px',
            width: 'auto',
            fontSize: '12px',
        },
    },
}));
const Dispute = ({ image, name, disabled, href }) => {
    console.log();
    const classes = useStyles({
        image,
        name,
        isMobile: window.innerWidth < 400,
    });
    return (
        <div className={classes.body}>
            <Card className={classes.card}>
                <img className={classes.img} src={image} alt='' />
                <CardContent className={classes.content}>
                    <Typography>{name}</Typography>
                    <Typography
                        variant='subtitle1'
                        color='textSecondary'
                    ></Typography>
                    <Button
                        className={classes.button}
                        variant='outlined'
                        disabled={disabled}
                        href={href}
                    >
                        S E L E C T
                    </Button>
                </CardContent>
            </Card>
        </div>
    );
};

export default Dispute;
