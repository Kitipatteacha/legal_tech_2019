import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-datepicker/dist/react-datepicker-cssmodules.min.css';

export default function CustomDatePicker(props) {
    const dateHandler = (date) => {
        props.answerHandler(props.label, date);
    };

    return (
        <div style={{ margin: 10 }}>
            <p>{props.label}</p>
            <DatePicker selected={props.answer} onChange={dateHandler} />
        </div>
    );
}
