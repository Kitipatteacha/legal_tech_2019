import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: '150px',
        '& .MuiInputBase-root': {
            color: 'white',
            '&:before': {
                borderColor: 'white',
            },
            '&:after': {
                borderColor: 'blue',
            },
            '&:hover:not(.Mui-disabled):before': {
                borderColor: 'blue',
            },
        },
        '& .MuiFormLabel-root.MuiInputLabel-root.MuiInputLabel-formControl.MuiInputLabel-animated.MuiInputLabel-shrink.MuiFormLabel-filled': {
            color: 'white',
        },
        '& .MuiSelect-icon': {
            color: 'white',
        },
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

export default function CustomSelect(props) {
    const classes = useStyles();
    const [choices, setChoice] = useState([]);
    const [label, setLabel] = useState('');

    useEffect(() => {
        setLabel(props.label);
        setChoice(props.choices);
    }, [props.choices, props.label]);

    const selectInputHandler = (event) => {
        props.answerHandler(props.label, event.target.value);
    };

    let choice_items = [];
    if (choices) {
        choice_items = choices.map((value) => (
            <MenuItem key={value} value={value}>
                {value}
            </MenuItem>
        ));
    }
    return (
        <div>
            <FormControl className={classes.formControl}>
                <InputLabel id='demo-simple-select-label'>{label}</InputLabel>
                <Select
                    labelId='demo-simple-select-label'
                    id='demo-simple-select'
                    value={props.answer}
                    onChange={selectInputHandler}
                >
                    {choice_items}
                </Select>
            </FormControl>
        </div>
    );
}
