import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    formControl: {
        minWidth: '150px',
        display: 'block',
        '& .MuiInputBase-root': {
            color: 'white',
            '&:before': {
                borderColor: 'white',
            },
            '&:after': {
                borderColor: 'blue',
            },
            '&:hover:not(.Mui-disabled):before': {
                borderColor: 'blue',
            },
        },
        '& .MuiFormLabel-root.MuiInputLabel-root.MuiInputLabel-formControl.MuiInputLabel-animated.MuiInputLabel-shrink.MuiFormLabel-filled': {
            color: 'white',
        },
        '& .MuiSelect-icon': {
            color: 'white',
        },
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));
export default function ValidationTextFields(props) {
    const classes = useStyles();
    const [error, setError] = useState(false);

    const inputHandler = (event) => {
        event.preventDefault();

        if (parseFloat(event.target.value) >= 0) {
            setError(false);
        } else {
            setError(true);
        }
        props.answerHandler(props.label, event.target.value);
    };

    // const submit = (event) => {
    //     event.preventDefault();
    // };

    return (
        <TextField
            className={classes.formControl}
            required
            error={error}
            id='standard-basic'
            label={props.question}
            value={props.answer}
            onChange={inputHandler}
            helperText={error && 'Incorrect input'}
        />
    );
}
