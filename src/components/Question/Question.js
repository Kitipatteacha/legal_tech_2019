import React, { useState, useEffect } from 'react';
import CustomSelect from './CustomSelect';
import CustomTextField from './CustomTextField';
import CustomDatePicker from './CustomDatePicker';
import Button from '@material-ui/core/Button';
import { Container } from '@material-ui/core';

export default function Question(props) {
    const [answers, setAnswers] = useState(null);

    useEffect(() => {
        const l = props.question_obj.inputs.map(({ label, default_answer }) => {
            return {
                label: label,
                answer: default_answer,
            };
        });
        setAnswers(l);
    }, [props.question_obj.inputs]);

    const answerHandler = (label, answer) => {
        const answers_temp = [...answers];
        for (let index in answers_temp) {
            if (answers_temp[index].label === label) {
                answers_temp[index].answer = answer;
            }
        }
        setAnswers(answers_temp);
    };

    const submitAnswer = () => {
        props.ansCollect(answers);
        setAnswers(null);
    };

    let inputs = [];
    if (answers) {
        inputs = props.question_obj.inputs.map((input, index) => {
            if (input.answer_type === 'select') {
                return (
                    <CustomSelect
                        key={input.label}
                        label={input.label}
                        answer={answers[index].answer}
                        answerHandler={answerHandler}
                        choices={input.choices}
                    />
                );
            } else if (input.answer_type === 'text') {
                return (
                    <CustomTextField
                        key={input.label}
                        label={input.label}
                        answer={answers[index].answer}
                        answerHandler={answerHandler}
                    />
                );
            } else if (input.answer_type === 'date_picker') {
                return (
                    <CustomDatePicker
                        key={input.label}
                        label={input.label}
                        answer={answers[index].answer}
                        answerHandler={answerHandler}
                    />
                );
            }
            return <p></p>;
        });
    }

    return (
        <Container>
            <div>
                <p style={{ fontSize: 'max(24px, 4vw)' }}>
                    {props.question_obj.question}
                </p>
                {inputs}
                <Button
                    variant='contained'
                    onClick={submitAnswer}
                    style={{ marginTop: 30, width: '100px' }}
                >
                    Next
                </Button>
            </div>
        </Container>
    );
}
